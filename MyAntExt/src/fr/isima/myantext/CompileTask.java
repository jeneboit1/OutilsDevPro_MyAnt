package fr.isima.myantext;

import java.io.IOException;

import fr.isima.myant.Task;

public class CompileTask extends Task {
	
	private String path;

	public CompileTask() {
		super();
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	
	public String getPath() {
		return path;
	}
	
	public void execute() {
		try {
			Runtime runtime = Runtime.getRuntime();
			runtime.exec("javac " + path);
			System.out.println("Compiled: " + path);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
