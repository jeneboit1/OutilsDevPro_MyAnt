package fr.isima.myantext;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import fr.isima.myant.Task;

public class DeleteTask extends Task {
	private String path;
	
	public DeleteTask() {
		super();
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	
	public String getPath() {
		return path;
	}
	
	@Override
	public void execute() {
		try {
			Files.delete(Paths.get(path));
			System.out.println("Deleted!");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
