package fr.isima.myant;

public class Main {

	public static void main(String[] args) {
		Project project = new Project("Projet 1");
		project.loadFromFile("mybuild.txt", args);
		project.execute();
	}
}
