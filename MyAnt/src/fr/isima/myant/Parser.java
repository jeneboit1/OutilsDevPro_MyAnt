package fr.isima.myant;

import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {
	
	private static final String COMMENT_REGEX = "^\\s*#.*";
	private static final String ECHO_REGEX = "\\s*echo\\s*:\\s*(?<msg>[,a-zA-Z0-9]*)";
	private static final String USE_REGEX = "\\s*use (?<class>[.A-Za-z0-9]+) as (?<alias>[a-zA-Z0-9]+)";
	private static final String TARGET_REGEX = "\\s*(?<name>[A-Za-z0-9]+)\\s*:\\s*(?<deps>[,a-zA-Z0-9\\s]*)";
	private static final String TARGET_DEP_REGEX = "\\s*(?<dep>[A-Za-z]+)\\s*,?";
	private static final String TASK_REGEX = "\\s*(?<name>[A-Za-z0-9]+)\\[(?<args>[^\\[\\]]*)\\]";
	private static final String TASK_ARG_REGEX = "\\s*(?<name>[A-Za-z]+)\\s*:\\s*\"(?<value>[^\"]+)\",?";
	
	public Parser() {
		
	}
	
	public List<Target> parseBuildFile(String path) {
		List<Target> targets = new ArrayList<Target>();
		Target target = new Target();
		boolean inTarget = false;
		try {
			BufferedReader in = new BufferedReader(new FileReader(path));
			String line;
			HashMap<String, String> aliases = new HashMap<String, String>();
			while((line = in.readLine()) != null) {
				if (!line.equals("") && !line.matches(COMMENT_REGEX)) {
					if (line.matches(USE_REGEX)) {
						parseUse(line, aliases);
					}
					else if (line.matches(ECHO_REGEX)) {
						System.out.println(parseEcho(line));
					}
					else if (line.matches(TARGET_REGEX)) {
						if (inTarget) {
							targets.add(new Target(target));
							target.getTasks().clear();
						}
						String name = parseTargetName(line);
						List<String> dependencies = parseTargetDependenciesName(line);
						target.setName(name);
						target.setDependencies(dependencies);
						inTarget = true;		
					}
					else if (inTarget){
						if (isKnownTask(line, aliases)) {
							parseTask(line, target, aliases);
						}
						else {
							System.out.println("Unknown command : " + line);
						}
					}
				}
			}
			if (inTarget) {
				targets.add(target);
			}
			in.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return targets;
	}
	
	public void parseUse(String line, Map<String, String> map) throws ParseException {
		Matcher matcher = Pattern.compile(USE_REGEX).matcher(line);
		if (!matcher.find()) {
			throw new ParseException("Line did not match pattern", 0);
		}
		
		String key = matcher.group("alias");
		String value = matcher.group("class");
		
		map.put(key, value);
	}
	
	public String parseEcho(String line) throws ParseException {
		Matcher matcher = Pattern.compile(ECHO_REGEX).matcher(line);
		if (!matcher.find()) {
			throw new ParseException("Line did not match pattern", 0);
		}
		String msg = matcher.group("msg");
		return msg;
	}
	
	public String parseTargetName(String line) throws ParseException {
		Matcher matcher = Pattern.compile(TARGET_REGEX).matcher(line);
		if (!matcher.find()) {
			throw new ParseException("Line did not match pattern", 0);
		}
		return matcher.group("name");
	}
	
	public List<String> parseTargetDependenciesName(String line) throws ParseException {
		List<String> results = new ArrayList<String>();
		
		Matcher matcher = Pattern.compile(TARGET_DEP_REGEX).matcher(line);
		if (!matcher.find()) {
			throw new ParseException("Line did not match pattern", 0);
		}
		
		while(matcher.find()) {
			results.add(matcher.group("dep"));
		}
		
		return results;
	}
	
	public boolean isKnownTask(String line, Map<String, String> aliases) {
		boolean res = false;
		try {
			String name = parseTaskName(line);
			if (name != null) {
				if (aliases.get(name) != null) {
					res = true;
				}
			}
		}
		catch (ParseException e) {
			e.printStackTrace();
		}
		return res;
	}
	
	public String parseTaskName(String line) throws ParseException {
		Matcher matcher = Pattern.compile(TASK_REGEX).matcher(line);
		if (!matcher.find()) {
			throw new ParseException("Line did not match pattern", 0);
		}
		return matcher.group("name");
	}
	
	public String parseTaskArguments(String line) throws ParseException {
		Matcher matcher = Pattern.compile(TASK_REGEX).matcher(line);
		if (!matcher.find()) {
			throw new ParseException("Line did not match pattern", 0);
		}
		return matcher.group("args");
	}
	
	// args is all the Task arguments separated by ','
	public void parseTask(String line, Target target, HashMap<String, String> alias) throws ParseException {
		Matcher matcher = Pattern.compile(TASK_REGEX).matcher(line);
		if (!matcher.find()) {
			throw new ParseException("Line did not match pattern", 0);
		}
		
		String name = matcher.group("name");
		String args = matcher.group("args");
		
		Matcher argsMatcher = Pattern.compile(TASK_ARG_REGEX).matcher(args);
		try {
			Class<?> c = Class.forName(alias.get(name));
			Constructor<?> constructor = c.getConstructor();
			Object o = constructor.newInstance();
			
			while(argsMatcher.find()) {
				String arg_name = argsMatcher.group("name");
				String arg_value = argsMatcher.group("value");
				String setter_name = "set" + arg_name.substring(0,1).toUpperCase() + arg_name.substring(1);
				Method method = c.getMethod(setter_name, String.class);
				method.invoke(o, arg_value);
			}
			target.addTask((Task)o);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
