package fr.isima.myant;

import java.util.ArrayList;
import java.util.List;

public class Project {
	private String name;
	private Target defaultTarget;
	private List<Target> targets = new ArrayList<Target>();

	public Project(String name) {
		this.name = name;
	}
	
	public Project(String name, List<Target> targets) {
		super();
		this.name = name;
		this.targets = targets;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Target getDefaultTarget() {
		return defaultTarget;
	}

	public void setDefaultTarget(Target defaultTarget) {
		this.defaultTarget = defaultTarget;
	}

	public List<Target> getTargets() {
		return targets;
	}

	public void setTargets(List<Target> targets) {
		this.targets = targets;
	}

	public void loadFromFile(String file, String[] args) {
		Parser parser = new Parser();
		List<Target> targets = parser.parseBuildFile(file);
		if(args.length == 0) {
			this.targets = Target.getExecutableTargets(targets, Target.getTarget(targets, "default"));
		}
		else {
			for(int i=0; i<args.length; i++) {
				String name = args[i];
				this.targets.addAll(Target.getExecutableTargets(targets, Target.getTarget(targets, name)));
			}
		}
	}
	
	public void execute() {
		for (Target target : targets) {
			target.execute();
		}
	}
}
