package fr.isima.myant;

public class EchoTask extends Task {
	private String message;
	
	public EchoTask()
	{
		super();
	}
	
	public EchoTask(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public void execute() {
		System.out.println(message);
	}
}
