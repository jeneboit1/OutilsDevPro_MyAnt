package fr.isima.myant;

import java.io.File;

public class MkdirTask extends Task {
	private String dir;
	
	public MkdirTask() {
		super();
	}
	
	public MkdirTask(String dir) {
		super();
		this.dir = dir;
	}

	public String getDir() {
		return dir;
	}

	public void setDir(String dir) {
		this.dir = dir;
	}

	@Override
	public void execute() {
		File theDir = new File(dir);
		
		if(!theDir.exists()) {
			System.out.println("Creating directory : " + theDir.getName());
			
			try {
				theDir.mkdir();
			} catch (SecurityException e) {
				System.out.println("Permission Denied");
			}
		} else {
			System.out.println("Dir already exists.");
		}
	}
}
