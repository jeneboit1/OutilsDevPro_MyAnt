package fr.isima.myant;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public class Target {
	private String name;
	private List<String> dependencies;
	private List<Task> tasks = new ArrayList<Task>();

	public Target(String name, List<String> dependencies, List<Task> tasks) {
		this.name = name;
		this.dependencies = dependencies;
		this.tasks = tasks;
	}

	public Target(String name) {
		this.name = name;
	}

	public Target() {

	}

	public Target(Target t) {
		this.name = t.getName();
		this.dependencies = t.getDependencies();
		this.tasks = new ArrayList<Task>(t.getTasks());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getDependencies() {
		return dependencies;
	}

	public void setDependencies(List<String> dependencies) {
		this.dependencies = dependencies;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	public void addTask(Task t) {
		tasks.add(t);
	}

	public void execute() {
		for (Task task : tasks) {
			task.execute();
		}
	}

	static public List<Target> getExecutableTargets(List<Target> targets, Target target) {
		List<Target> executableTargets = new ArrayList<Target>();
		try {
			for(int i=0; i<target.getDependencies().size(); i++) {
				//final int j = i;
				//Optional<Target> optTarget = targets.stream().filter((t) -> t.getName().equals(target.getDependencies().get(j))).findFirst();
				//Target depTarget = optTarget.get();

				Target depTarget = null;
				for (int j=0; j<targets.size(); j++) {
					if (targets.get(j).name.equals(target.getDependencies().get(j))) {
						depTarget = targets.get(j);
						break;
					}
				}

				if (depTarget.getDependencies().size() > 0) {
					executableTargets.addAll(getExecutableTargets(targets, depTarget));
				}
				else {
					executableTargets.add(depTarget);
				}
			}
			executableTargets.add(target);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return executableTargets;
	}

	static public Target getTarget(List<Target> targets, String name) {
		for(Iterator<Target> i=targets.iterator(); i.hasNext();) {
			Target target = i.next();
			if(target.getName().equals(name)) {
				return target;
			}
		}
		return null;
	}
}
