package fr.isima.myant;
import static java.nio.file.StandardCopyOption.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CopyTask extends Task {
	private String from;
	private String to;
	
	public CopyTask() {
		super();
	}
	
	public CopyTask(String from, String to) {
		super();
		this.from = from;
		this.to = to;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	@Override
	public void execute() {
		try {
			Files.copy(Paths.get(from), Paths.get(to), REPLACE_EXISTING);
			System.out.println("Copied !");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
