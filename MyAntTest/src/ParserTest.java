import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import fr.isima.myant.Parser;
import fr.isima.myant.Target;
import fr.isima.myant.Task;

public class ParserTest {

	Parser parser = new Parser();

	@Test
	public void TestUse() {
		Map<String, String> map = new HashMap<String, String>();
		try {
			parser.parseUse("use task as t", map);
			assertEquals(map.get("t"), "task");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void TestEcho() {
		try {
			assertEquals(parser.parseEcho("echo : echo"), "echo");
		}
		catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void TestTargetName() {
		try {
			assertEquals(parser.parseTargetName("Target:"), "Target");
			assertEquals(parser.parseTargetName(" Target : "), "Target");
			assertEquals(parser.parseTargetName("Target : default"), "Target");
		}
		catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void TestTargetDependenciesName() {
		try {
			List<String> dep = parser.parseTargetDependenciesName("target : default, compile");
			assertEquals(dep.size(), 2);
			assertEquals(dep.get(0), "default");
			assertEquals(dep.get(1), "compile");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void TestKnownTask() {
		Map<String, String> map = new HashMap<String, String>();
		try {
			parser.parseUse("use task as t", map);
			assertEquals(parser.isKnownTask("t[]", map), true);
			assertEquals(parser.isKnownTask("t1[]", map), false);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void TestTaskName() {
		try {
			assertEquals(parser.parseTaskName("Task[]"), "Task");
		}
		catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void TestTaskArguments() {
		try {
			assertEquals(parser.parseTaskArguments("Task[]"), "");
			assertEquals(parser.parseTaskArguments("Task[arg1: \"argument\"]"), "arg1: \"argument\"");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void TestTask() {
		String line = "MyEcho[message: \"hello\"]";
		Target target = new Target();
		HashMap<String, String> alias = new HashMap<>();
		alias.put("MyEcho", "fr.isima.myant.EchoTask");
		try {
			parser.parseTask(line, target, alias);
			assertEquals(target.getTasks().size(), 1);
		}
		catch (ParseException e) {
			e.printStackTrace();
		}
	}
}
